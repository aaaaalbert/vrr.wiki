# Installing VRR's Musician interface
In order to use a Virtual Rehearsal Room and play with others,
you need VRR's Musician interface installed on your computer first.

* Download [`musician.zip`](https://git.iem.at/cm/vrr/-/jobs/17223/artifacts/download). (This zip file also includes all of the Pure Data externals VRR depends on.)
* Extract the zip file into a working directory.

To start the Musician interface, launch Pure Data and open `VRR/musician.pd`
from your working directory.

Read on to learn how to [configure the Musician interface](config.md)
for streaming.

